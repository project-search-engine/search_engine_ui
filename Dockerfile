FROM alpine:3.7
RUN apk add --no-cache python3 \
    && rm -rf /var/cache/apk/*
WORKDIR /search_engine_ui
COPY requirements.txt /search_engine_ui
RUN pip3 install -r requirements.txt
COPY . /search_engine_ui
ENV FLASK_APP=ui.py
EXPOSE 8000
WORKDIR /search_engine_ui/ui
CMD ["gunicorn", "ui:app", "-b", "0.0.0.0"]
